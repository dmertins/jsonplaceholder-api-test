import unittest

import requests

from tests.urls import POSTS_URL, POST_URL, POST_COMMENTS_URL
from tests.utils import assert_is_json_schema_valid, get_json_schema, \
    assert_json_object_field_in_list_equal


class TestPostsAPIContract(unittest.TestCase):
    """Test the Posts API endpoints contracts."""

    def test_get_posts(self):
        """Test the endpoint for fetching all posts."""
        response = requests.get(POSTS_URL)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        posts_list_schema = get_json_schema('posts_list_schema')
        assert_is_json_schema_valid(response.json(), posts_list_schema)

    def test_get_posts_with_filter(self):
        """Test the endpoint for fetching all posts filtering resources
        with query parameters.
        """
        user_id = 1
        payload = {'userId': user_id}
        response = requests.get(POSTS_URL, params=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        posts_list_schema = get_json_schema('posts_list_schema')
        assert_is_json_schema_valid(response.json(), posts_list_schema)

        posts_list = response.json()
        assert_json_object_field_in_list_equal(posts_list, 'userId', user_id)

    def test_get_posts_with_filter_empty_list(self):
        """Test the endpoint for fetching all posts filtering resources
        with query parameters and receiving an empty list.
        """
        nonexistent_user_id = 11
        payload = {'userId': nonexistent_user_id}
        response = requests.get(POSTS_URL, params=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        posts_list = response.json()
        self.assertListEqual(posts_list, [])

    def test_post_posts(self):
        """Test the endpoint for creating a post."""
        payload = {'title': 'foo', 'body': 'bar', 'userId': 1}
        response = requests.post(POSTS_URL, json=payload)

        self.assertEqual(response.status_code, requests.codes.created,
                         f'Expected status code: {requests.codes.created}. '
                         f'Returned: {response.status_code}')

        post_schema = get_json_schema('post_schema')
        assert_is_json_schema_valid(response.json(), post_schema)

        post = response.json()
        self.assertEqual(post['title'], 'foo')
        self.assertEqual(post['body'], 'bar')
        self.assertEqual(post['userId'], 1)

    def test_post_posts_for_all_registered_users(self):
        """Test the endpoint for creating a post for all registered
        users.
        """
        for user_id in range(1, 11):
            with self.subTest(user_id=user_id):
                payload = {'title': 'foo', 'body': 'bar', 'userId': user_id}
                response = requests.post(POSTS_URL, json=payload)

                self.assertEqual(
                    response.status_code, requests.codes.created,
                    f'Expected status code: {requests.codes.created}. '
                    f'Returned: {response.status_code}'
                )

                post_schema = get_json_schema('post_schema')
                assert_is_json_schema_valid(response.json(), post_schema)

                post = response.json()
                self.assertEqual(post['title'], 'foo')
                self.assertEqual(post['body'], 'bar')
                self.assertEqual(post['userId'], user_id)

    def test_get_post(self):
        """Test the endpoint for fetching a specific post."""
        response = requests.get(POST_URL.format(postId=1))

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        post_schema = get_json_schema('post_schema')
        assert_is_json_schema_valid(response.json(), post_schema)

    def test_get_post_not_found(self):
        """Test the endpoint for fetching a specific post with a
        nonexistent postId.
        """
        nonexistent_post_id = 101
        response = requests.get(POST_URL.format(postId=nonexistent_post_id))

        self.assertEqual(response.status_code, requests.codes.not_found,
                         f'Expected status code: {requests.codes.not_found}. '
                         f'Returned: {response.status_code}')

        self.assertDictEqual(response.json(), {})

    def test_put_post(self):
        """Test the endpoint for updating a specific post."""
        payload = {'title': 'foo', 'body': 'bar', 'userId': 100}
        response = requests.put(POST_URL.format(postId=1), json=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        post_schema = get_json_schema('post_schema')
        assert_is_json_schema_valid(response.json(), post_schema)

        post = response.json()
        self.assertEqual(post['title'], 'foo')
        self.assertEqual(post['body'], 'bar')
        self.assertEqual(post['userId'], 100)

    def test_patch_post(self):
        """Test the endpoint for partially updating a specific post."""
        payload = {'title': 'foo'}
        response = requests.patch(POST_URL.format(postId=1), json=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        post_schema = get_json_schema('post_schema')
        assert_is_json_schema_valid(response.json(), post_schema)

        post = response.json()
        self.assertEqual(post['title'], 'foo')

    def test_delete_post(self):
        """Test the endpoint for deleting a specific post."""
        response = requests.delete(POST_URL.format(postId=1))

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        self.assertDictEqual(response.json(), {})

    def test_get_post_comments(self):
        """Test the endpoint for fetching all comments of a post."""
        post_id = 1
        response = requests.get(POST_COMMENTS_URL.format(postId=post_id))

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        comments_list_schema = get_json_schema('comments_list_schema')
        assert_is_json_schema_valid(response.json(), comments_list_schema)

        comments_list = response.json()
        assert_json_object_field_in_list_equal(comments_list,
                                               'postId',
                                               post_id)

    def test_get_post_comments_with_filter(self):
        """Test the endpoint for fetching all comments of a post
        filtering resources with query parameters.
        """
        post_id = 1
        email = 'Eliseo@gardner.biz'
        payload = {'email': email}
        response = requests.get(POST_COMMENTS_URL.format(postId=post_id),
                                params=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        comments_list_schema = get_json_schema('comments_list_schema')
        assert_is_json_schema_valid(response.json(), comments_list_schema)

        comments_list = response.json()
        assert_json_object_field_in_list_equal(comments_list,
                                               'email',
                                               email)

    def test_get_post_comments_with_filter_empty_list(self):
        """Test the endpoint for fetching all comments of a post
        filtering resources with query parameters and receiving an empty
        list.
        """
        payload = {'email': 'nonexistent@email.com'}
        response = requests.get(POST_COMMENTS_URL.format(postId=1),
                                params=payload)

        self.assertEqual(response.status_code, requests.codes.ok,
                         f'Expected status code: {requests.codes.ok}. '
                         f'Returned: {response.status_code}')

        comments_list = response.json()
        self.assertListEqual(comments_list, [])


if __name__ == '__main__':
    unittest.main()
