import json
from typing import Dict, List, NoReturn, Optional, Union, Any

from jsonschema import validate, ValidationError

from tests.settings import JSON_SCHEMAS_PATH, JSON_FILE_EXT


def get_json_schema(file_name: str) -> Dict:
    """Returns the json schema for the specified file name."""
    schema_path = JSON_SCHEMAS_PATH / (file_name + JSON_FILE_EXT)
    with schema_path.open(mode='r', encoding='utf-8') as schema_file:
        json_schema = json.load(schema_file)
    return json_schema


Json = Union[Dict, List]


def assert_is_json_schema_valid(instance: Json,
                                schema: Json) -> Optional[NoReturn]:
    """Raises AssertionError if json instance don't match the schema."""
    try:
        validate(instance, schema)
    except ValidationError:
        raise AssertionError(f'JSON schema validation failed.')


def assert_json_object_field_in_list_equal(json_list: List,
                                           field: str,
                                           value: Any) -> Optional[NoReturn]:
    """Raises AssertionError if any object in the list contains the
    specified field with a value different than specified."""
    for obj in json_list:
        assert obj[field] == value, f'Expected {field}: {value}. ' \
                                    f'Found: {obj[field]}. ' \
                                    f'In: {obj}'
