import os
import pathlib

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR_PATH = pathlib.Path(BASE_DIR)

JSON_SCHEMAS_PATH = BASE_DIR_PATH / 'tests' / 'jsonschemas'
JSON_FILE_EXT = '.json'
