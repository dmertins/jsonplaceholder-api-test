BASE_URL = 'https://jsonplaceholder.typicode.com'
RESOURCE_SEPARATOR = '/'

POSTS_URL = RESOURCE_SEPARATOR.join([BASE_URL, 'posts'])
POST_URL = RESOURCE_SEPARATOR.join([POSTS_URL, '{postId}'])
POST_COMMENTS_URL = RESOURCE_SEPARATOR.join([POST_URL, 'comments'])
