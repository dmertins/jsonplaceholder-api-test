import pathlib
import sys
from datetime import datetime
from unittest import defaultTestLoader, TextTestRunner

from HtmlTestRunner import HTMLTestRunner

REPORTS_DIR = 'reports'
HTML_DIR = 'html'
TXT_DIR = 'txt'
REPORT_NAME = 'test_results'
DEFAULT_REPORT_TYPE = 'html'


def main():
    report_type = DEFAULT_REPORT_TYPE

    try:
        subcommand = sys.argv[1]
        if subcommand == '-o':
            if sys.argv[2] in ['html', 'txt']:
                report_type = sys.argv[2]
    except IndexError:
        pass

    suite = defaultTestLoader.discover(start_dir='tests', pattern='test_*.py')

    if report_type == 'html':
        run_html_test_runner(suite)
    elif report_type == 'txt':
        run_text_test_runner(suite)
    else:
        raise ValueError('Invalid report_type: ' + report_type)


def run_text_test_runner(suite):
    time_format = '%Y-%m-%d_%H-%M-%S'
    timestamp = datetime.now().strftime(time_format)
    report_file = REPORT_NAME + '_' + timestamp + '.txt'
    reports_dir_path = pathlib.Path(REPORTS_DIR)
    report_file_path = reports_dir_path / TXT_DIR / report_file

    report_file_path.parent.mkdir(parents=True, exist_ok=True)
    with report_file_path.open(mode='w', encoding='utf-8') as file:
        print('Running tests...')
        runner = TextTestRunner(stream=file, verbosity=2)
        runner.run(suite)
        print('Generating text reports...')
        print(file.name)


def run_html_test_runner(suite):
    html_runner = HTMLTestRunner(output=REPORTS_DIR + '/' + HTML_DIR,
                                 combine_reports=True,
                                 report_name=REPORT_NAME)
    html_runner.run(suite)


if __name__ == '__main__':
    main()
