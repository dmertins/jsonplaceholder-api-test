# JSONPlaceholder API Test

This project tests the endpoints of the public API [JSONPlaceholder](https://jsonplaceholder.typicode.com).

JSONPlaceholder is a fake online REST API designed for testing and prototyping.

All HTTP methods are supported, although the resources are not actually created, updated or deleted on the server. The API just fakes it, responding as if they were.

This project tests all routes from the /posts resource:
- GET       /posts
- GET 	    /posts?userId={userId}
- POST 	    /posts
- GET 	    /posts/{postId}
- PUT 	    /posts/{postId}
- PATCH 	/posts/{postId}
- DELETE    /posts/{postId}
- GET 	    /posts/{postId}/comments
- GET 	    /posts/{postId}/comments?email={email}

## Requirements

[Python 3.8+](https://www.python.org/downloads/).

## Technologies used

### [unittest](https://docs.python.org/3/library/unittest.html)

Python's built-in unit testing framework. Used as a framework to write the test cases and as the test runner for running the tests and showing the results.

### [requests](https://requests.readthedocs.io/en/master/)

An HTTP library for Python. Used to handle the API requests and responses.

### [jsonschema](https://pypi.org/project/jsonschema/)

An implementation of [JSON Schema](https://json-schema.org/) for Python. Used to validate the json responses from the API.

### [html-testRunner](https://pypi.org/project/html-testRunner/)

HtmlTest runner is a unittest test runner that save test results in Html files, for human readable presentation of results.

## Setting the environment

To create and activate a python virtual environmnent on Linux:
```bash
$ python -m venv ./venv && source ./venv/bin/activate
```

To create and activate a python virtual environmnent on Windows:
```
\> python -m venv ./venv && venv\Scripts\activate
```

To install the project dependencies:
```bash
(venv) $ pip install -r requirements.txt
```

To deactivate the python virtual environmnent:
```bash
(venv) $ deactivate
```

## Running the tests

To run all the test modules:

```bash
(venv) $ python -m unittest
```

To run specific test modules:
```bash
(venv) $ python -m unittest <test_module1> <test_module2>
```

To run a single test class:
```bash
(venv) $ python -m unittest <test_module>.<TestClass>
```

To run an individual test method:
```bash
(venv) $ python -m unittest <test_module>.<TestClass>.<test_method>
```

Example:
```bash
(venv) $ python -m unittest tests.test_posts_api.TestPostsAPIContract.test_get_posts
```

## Test output

The default output of the test runner will show a detailed description only for the tests that fail:
```commandline
......F.....
======================================================================
FAIL: test_get_posts (tests.test_posts_api.TestPostsAPIContract)
Test the endpoint for fetching all posts.
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/dmertins/Programming/jsonplaceholder-api-test/tests/test_posts_api.py", line 17, in test_get_posts
    self.assertEqual(response.status_code, requests.codes.ok,
AssertionError: 400 != 200 : Expected status code: 200. Returned: 400

----------------------------------------------------------------------
Ran 12 tests in 4.584s

FAILED (failures=1)
```

Tests can be run with more detail (higher verbosity) by passing in the ```-v``` flag:
```bash
(venv) $ python -m unittest -v
test_delete_post (tests.test_posts_api.TestPostsAPIContract)
Test the endpoint for deleting a specific post. ... ok
test_get_post (tests.test_posts_api.TestPostsAPIContract)
Test the endpoint for fetching a specific post. ... ok
test_get_post_comments (tests.test_posts_api.TestPostsAPIContract)
Test the endpoint for fetching all comments of a post. ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.865s

OK
```

## Test report

Tests report files can be generated in ```html``` or ```txt``` format. The default report type is ```html```. 

Running the ```testrunner.py``` module saves the test report in ```/reports/html``` directory:
```bash
(venv) $ python testrunner.py
```

This is the same as running ```testrunner.py``` passing the ```-o``` argument with ```html``` value:
```bash
(venv) $ python testrunner.py -o html
```

The ```txt``` report type is generated in ```/reports/txt``` by passing the ```-o``` argument with ```txt``` value:
```bash
(venv) $ python testrunner.py -o txt
```
